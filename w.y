
%{
#include "symtab.h"

typedef struct 
{
 char thestr[2000];
 char strlit[2000];
 int ival;
 int ttype;
}tstruct ; 

#define YYSTYPE  tstruct 



int yylex();
void yyerror( char *s );
int GetTypeFromMatch(int type1, int type2);





%}

%token tstart
%token tfinish
%token tcreate
%token tint    
%token thoriz
%token tvert
%token tassign  
%token tstrlit  
%token tid  
%token tnum  
%token tstring

%define locations

%%

prog : tstart DL SL tfinish
                          {
                          showtab();
                   		FILE * fp;
   						int i;
   						/* open the file for writing*/
   						fp = fopen ("out.c","w");
			    		fprintf(fp,"#include <stdio.h>\n");
			    		fprintf(fp,"#include <stdlib.h>\n");
			    		fprintf(fp,"#include <string.h>\n");
			    		fprintf(fp,"\n");
		            	fprintf(fp,"int main()\n");
			    		fprintf(fp,"{\n");
			    		fprintf(fp,"%s", $2.thestr);
			    		fprintf(fp,"%s", $3.thestr);
			    		fprintf(fp,"return 0;\n");
			    		fprintf(fp,"}\n");
			    		fclose (fp);
			  }
 ;

DL :  DL D   {sprintf($$.thestr, "%s%s", $1.thestr, $2.thestr);}
  | D        {sprintf($$.thestr, "%s", $1.thestr);	       }
  ;


D : tcreate type tid ';' { addtab($3.thestr);
			   addtype($3.thestr, $2.ttype);
			   $$.ttype = $2.ttype;
                           if($2.ttype == TYPE_INTEGER) 
			     {sprintf($$.thestr,"int %s;\n",$3.thestr);}
			   else if($2.ttype == TYPE_STRING)
			     {sprintf($$.thestr,"char * %s;\n",$3.thestr);}
			 } 

type: tint {$$.ttype = TYPE_INTEGER;} | tstring {$$.ttype = TYPE_STRING;};

SL :  SL S   {sprintf($$.thestr, "%s%s", $1.thestr, $2.thestr);}
  | S        {sprintf($$.thestr, "%s", $1.thestr);}
  ;

S :  thoriz tid ';'      
              {
                if ( intab($2.thestr) )
                   {
		     if(gettype($2.thestr) == TYPE_STRING)
			sprintf($$.thestr,"printf(\"%cs\\n\",%s);\n",'%',$2.thestr);
		     else if(gettype($2.thestr) == TYPE_INTEGER)
			sprintf($$.thestr,"printf(\"%cd\\n\",%s);\n",'%',$2.thestr);
		   }
                else{
                   printf("***ERROR*** : UNDECLARED VARIABLE : (%s on line %d)\n", $2.thestr, yyloc.first_line);
					return 1;
					}
              }
  | tvert tid ';'
		{
			if( intab($2.thestr) )
			{
				
				{	if(gettype($2.thestr) == TYPE_STRING)
					{
						sprintf($$.thestr, "for(int ITERATOR = 0; ITERATOR < strlen(%s); ITERATOR++)\n", $3.thestr);
						char temp [2000];
						sprintf(temp, "{\nprintf(\"%cc\\n\",%s[ITERATOR]);\n}\n",'%',$2.thestr);
						strcat($$.thestr, temp);
						strcat($$.thestr, "\n");
					}
					else if (gettype($2.thestr) == TYPE_INTEGER)
					{
						printf("***ERROR***: On line %d : Only strings may be printed vertically\n", yyloc.first_line);
						return 1;
					}
				}
			}
		else{
		
		   printf("***ERROR*** : UNDECLARED VARIABLE : (%s on line %d)\n", $2.thestr, yyloc.first_line);
					return 1;
					}
		}
  |  tid tassign expr ';'  
              {
               /* printf("assign\n");*/
                if ( intab($1.thestr) )
                {
                } 
                else
             	{
                   printf("***ERROR*** : UNDECLARED VARIABLE : (%s on line %d)\n", $2.thestr, yyloc.first_line);
					return 1;
                }
                	$1.ttype = gettype($1.thestr);
                	if ($1.ttype > 0 )
                	{
                  		if ($1.ttype == $3.ttype)
                  		{
                        		 if(TYPE_STRING == $1.ttype)
                        		 {
                        		 	sprintf($$.thestr,"%s = %s;\n", $1.thestr, $3.thestr);
                        		 }
                        		 else
                        		 {
                        		 	sprintf($$.thestr,"%s = %s;\n", $1.thestr, $3.thestr);         	 	
                           		 }
                  		}
                  		else
                     		{
                      			printf("Incompatible ASSIGN types %d %d\n", $1.ttype, $3.ttype);
					return 1;
                     		}
                	}
                	else
                	{ 
                    	yyerror("Type Error :::");
						return 1;
                    }
	      }         
  | error ';'    {printf("error in statement on line %d\n", yyloc.first_line);
					return 1;
				
				}
  ;

expr : expr '+' term 
             { 
               $$.ttype = GetTypeFromMatch($1.ttype,$3.ttype); 
               sprintf($$.thestr,"%s + % s",$1.thestr,$3.thestr);
	     }
  |  expr '-' term
	     {
	       		$$.ttype = GetTypeFromMatch($1.ttype,$3.ttype);
	       		sprintf($$.thestr,"%s - % s",$1.thestr,$3.thestr); 
	     }
  |  term      { 
		strcpy($$.thestr, $1.thestr);
		$$.ival = $1.ival;
		$$.ttype = $1.ttype; }
  ;

term : term '*' factor
             { 
               $$.ttype = GetTypeFromMatch($1.ttype,$3.ttype); 
               sprintf($$.thestr,"%s * % s",$1.thestr,$3.thestr);
             }
  | term '/' factor
	  {
	       $$.ttype = GetTypeFromMatch($1.ttype,$3.ttype); 
	       sprintf($$.thestr,"%s / % s",$1.thestr,$3.thestr);
	  }
  | factor   { 
		strcpy($$.thestr, $1.thestr);
		$$.ival = $1.ival;
		$$.ttype = $1.ttype; }
  ;

factor : tid  
              {
                if ( intab($1.thestr) )
                {
                }
                else
                   printf("UNDECLARED:: %s \n", $1.thestr);
                $$.ttype = gettype($1.thestr);
                if ($$.ttype > 0 )
                      sprintf($$.thestr,$1.thestr);
                else {
                    yyerror("Type Error :::");
					return 1;
					}
              }
 | tnum     {$$.ttype = TYPE_INTEGER;
	     sprintf($$.thestr, "%s", $1.thestr);}
 | tstrlit  {$$.ttype = TYPE_STRING;
	     sprintf($$.thestr, "%s", $1.thestr);}
 ;

%%
int GetTypeFromMatch(int type1, int type2)
{
	int type;
	if (type1 == TYPE_INTEGER && type2 == TYPE_INTEGER)
	{
		type = TYPE_INTEGER;
	} 
/*    else if (type1 == TYPE_FLOAT && type2 == TYPE_FLOAT)  
    {
    	type = TYPE_FLOAT;
    }
    else if((type1 == TYPE_INTEGER && type2 == TYPE_FLOAT) ||\
    		(type1 == TYPE_FLOAT && type2 == TYPE_INTEGER) )
    {
    	type = TYPE_FLOAT;
    } *///future
    else 
    {
    	type = -1;
    }
	return type;
}
int main(int argc, char **argv)
{
	if(argc > 1)
	{
		switch(*argv[1])
		{
			case '-':
				if(argv[1][1] == 't')
				{
  					printf("Compiler Tracing enabled!\n");		
					enabletracing();
				}
		    break;
		    default:
		    break;
		}
  	}
  yyparse ();
  system("gcc out.c"); 
}
