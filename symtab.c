#include "symtab.h"

entry symtab[100];
bool trace = false;
int nsym;

char * GetType2Str(int a)
{
	switch(a)
	{
		case TYPE_INTEGER:
			return "INTEGER";		
		break;
		case TYPE_STRING:
			return "STRING";	
		break;
	}
}

void enabletracing(void)
{
	trace = true;
}
bool istracingenabled(void)
{
	return trace;
}
void addtab( char *s)
{
 nsym++;
 strcpy( symtab[nsym].sname, s);
 symtab[nsym].stype = -1;
}

void showtab(void)
{
 
if(istracingenabled()) {
 int i;
 for (i = 1; i <= nsym; ++i)
   printf("%d: %s %d\n", i, symtab[i].sname, symtab[i].stype);
	}
}

int intab( char *s)
{
 int i;
 for ( i = 1; i <= nsym; ++i)
 {
   if ( strcmp(symtab[i].sname, s) == 0)
    return 1;
 }
 return 0;

}
int addtype( char *s, int t)
{
 int i, loc = -1;
 for ( i = 1; i <= nsym; ++i)
 {
   if ( strcmp(symtab[i].sname, s) == 0)
    loc = i;
 }
 if (loc > 0)
  {
  
	if(istracingenabled()) {
	printf("Set type %s to %s\n", s, GetType2Str(t));
	}
   
   symtab[loc].stype = t; 
  }
 else
 {
   printf("Unable to set type %s to %s\n", s, GetType2Str(t));
 } 
}


int gettype( char *s)
{
 int t = -1;
 int i, loc = -1;
 for ( i = 1; i <= nsym; ++i)
 {
   if ( strcmp(symtab[i].sname, s) == 0)
    loc = i;
 }
 if (loc > 0)
  {
   t = symtab[loc].stype;
  
 if(istracingenabled()) {
	printf("Get type for %s to %s\n", s, GetType2Str(t));
	}

  }
  if(istracingenabled())
  {
 	if (loc <= 0)
 	{
   		printf("gettype var %s not found\n", s);
   	}
 	else if (t < 0)
 	{
   		printf("gettype var %s has bad type %s\n", s, GetType2Str(t));
   	}
 	else
 	{
 		 printf("gettype var %s has type %s\n", s, GetType2Str(t));
    }
  }	 
 return t;
}



